from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

from track import views

urlpatterns = patterns('',
    url(r'^$', views.index, name='index'),
    url(r'^student/(?P<survey_id>\d+)/$', views.student, name='student'),
    url(r'^overview', views.overview, name='overview'),

    # Examples:
    # url(r'^$', 'tracker.views.home', name='home'),
    # url(r'^tracker/', include('tracker.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
)
