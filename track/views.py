# Create your views here.
from django.http import HttpResponse
from models import *
from django.http import Http404
from django.shortcuts import render
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

def index(request):
    try:
        surveys = CompletedSurveys.objects.all()
    except Exception as e:
        print e
        raise Http404
    return render(request, 'track/index.html', {'surveys':surveys})


def overview(request):
    allSurveys = CompletedSurveys.objects.all()

    surveysByProcedure = dict()

    for survey in allSurveys:
        if str(survey.q7) is not "" and survey.q7 is not None:
            answer = LimeAnswers.objects.get(qid=7, code=survey.q7)
            if answer.answer in surveysByProcedure:

                alreadyStoredProcedureSurveys = surveysByProcedure[answer.answer]
                if survey.q1 not in alreadyStoredProcedureSurveys:
                    alreadyStoredProcedureSurveys.append(survey)
                    surveysByProcedure[answer.answer] = alreadyStoredProcedureSurveys
            else :
                procedureSurveys = []
                procedureSurveys.append(survey)
                surveysByProcedure[answer.answer] = procedureSurveys

    # for surveyTitle in surveysByProcedure:
    #     print '\n'
    #     print surveyTitle
    #     for survey in surveysByProcedure[surveyTitle]:
    #         print survey.q1

    return render(request, 'track/overview.html', {'surveysByProcedure': surveysByProcedure})


def student(request, survey_id):
    search = None
    if request.method == 'POST':
        search = request.POST.get('search', '')
    try:
        if search is not None:
            survey = CompletedSurveys.objects.get(id=survey_id)
            surveys = CompletedSurveys.objects.exclude(submitdate__isnull=True).filter(q18__iexact=search)
        else:
            survey = CompletedSurveys.objects.get(id=survey_id)
            surveys = CompletedSurveys.objects.exclude(submitdate__isnull=True)
        page = request.GET.get('page')
        p = Paginator(surveys, 10)
        s = p.page(page)

    except PageNotAnInteger:
        print "PageNotAnInteger"
        s = p.page(1)
    except EmptyPage:
        print "EmptyPage"
        # If page is out of range (e.g. 9999), deliver last page of results.
        s = p.page(1)
    except Exception as e:
        print e
        raise Http404
    for srv in s:
        srv.procedure()
    return render(request, 'track/survey.html',
                  {'surveys':s, 'current':survey, 'data':survey.answerData(), 
                  'prev': int(survey_id)-1, 'next': int(survey_id)+1})
