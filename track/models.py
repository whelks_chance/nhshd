# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#     * Rearrange models' order
#     * Make sure each model has one field with primary_key=True
# Feel free to rename the models, but don't rename db_table values or field names.
#
# Also note: You'll have to insert the output of 'django-admin.py sqlcustom [appname]'
# into your database.

from django.db import models

class LimeAnswers(models.Model):
    qid = models.IntegerField(primary_key=True)
    code = models.CharField(max_length=15, primary_key=True)
    answer = models.TextField()
    sortorder = models.IntegerField()
    assessment_value = models.IntegerField()
    language = models.CharField(max_length=60, primary_key=True)
    scale_id = models.IntegerField(primary_key=True)
    class Meta:
        db_table = u'lime_answers'

class LimeQuestions(models.Model):
    qid = models.IntegerField(primary_key=True)
    parent_qid = models.IntegerField()
    sid = models.IntegerField()
    gid = models.IntegerField()
    type = models.CharField(max_length=3)
    title = models.CharField(max_length=60)
    question = models.TextField()
    preg = models.TextField(blank=True)
    help = models.TextField(blank=True)
    other = models.CharField(max_length=3)
    mandatory = models.CharField(max_length=3, blank=True)
    question_order = models.IntegerField()
    language = models.CharField(max_length=60, primary_key=True)
    scale_id = models.IntegerField()
    same_default = models.IntegerField()
    relevance = models.TextField(blank=True)
    class Meta:
        db_table = u'lime_questions'

class CompletedSurveys(models.Model):
    id = models.IntegerField(primary_key=True)
    token = models.CharField(max_length=108, blank=True)
    submitdate = models.DateTimeField(null=True, blank=True)
    lastpage = models.IntegerField(null=True, blank=True)
    startlanguage = models.CharField(max_length=60)
    startdate = models.DateTimeField()
    datestamp = models.DateTimeField()
    ipaddr = models.TextField(blank=True)
    def answerData(self):
        data = []
        q1 = LimeQuestions.objects.get(qid=1)
        data.append({'question':q1.question,'answer':self.q1})
        q2 = LimeQuestions.objects.get(qid=2)
        q2a = LimeAnswers.objects.get(qid=2, code=self.q2)
        data.append({'question':q2.question,'answer':q2a.answer})
        q3 = LimeQuestions.objects.get(qid=3)
        data.append({'question':q3.question,'answer':self.q3})
        q4 = LimeQuestions.objects.get(qid=4)
        data.append({'question':q4.question,'answer':self.q4})
        q5 = LimeQuestions.objects.get(qid=5)
        data.append({'question':q5.question,'answer':self.q5})
        q6 = LimeQuestions.objects.get(qid=6)
        if (self.q6 == '-oth-'):
            data.append({'question':q6.question,'answer':self.q6other})
        else:
            q6a = LimeAnswers.objects.get(qid=6, code=self.q6)
            data.append({'question':q6.question,'answer':q6a.answer})
        q7 = LimeQuestions.objects.get(qid=7)
        q7a = LimeAnswers.objects.get(qid=7, code=self.q7)
        data.append({'question':q7.question, 'answer':q7a.answer})
        q8 = LimeQuestions.objects.get(qid=8)
        q8a = LimeAnswers.objects.get(qid=8, code=self.q8SQ001)
        data.append({'question':q8.question, 'answer':q8a.answer})
        q9 = LimeQuestions.objects.get(qid=11)
        q9a = LimeAnswers.objects.get(qid=10, code=self.q10SQ001)
        data.append({'question':q9.question, 'answer':q9a.answer})
        q10 = LimeQuestions.objects.get(qid=12)
        q10a = LimeAnswers.objects.get(qid=10, code=self.q10SQ002)
        data.append({'question':q10.question, 'answer':q10a.answer})
        q11 = LimeQuestions.objects.get(qid=13)
        q11a = LimeAnswers.objects.get(qid=10, code=self.q10SQ003)
        data.append({'question':q11.question, 'answer':q11a.answer})
        q12 = LimeQuestions.objects.get(qid=14)
        q12a = LimeAnswers.objects.get(qid=10, code=self.q10SQ004)
        data.append({'question':q12.question, 'answer':q12a.answer})
        q13 = LimeQuestions.objects.get(qid=15)
        q13a = LimeAnswers.objects.get(qid=10, code=self.q10SQ005)
        data.append({'question':q13.question, 'answer':q13a.answer})
        q14 = LimeQuestions.objects.get(qid=16)
        q14a = LimeAnswers.objects.get(qid=10, code=self.q10SQ006)
        data.append({'question':q14.question, 'answer':q14a.answer})
        q15 = LimeQuestions.objects.get(qid=17)
        data.append({'question':q15.question, 'answer':self.q17})

        return data        

    proc = None
    def procedure(self):
        try:
            q7a = LimeAnswers.objects.get(qid=7, code=self.q7)
            self.proc = q7a.answer
            return q7a.answer
        except Exception:
            return None        
        
    q1 = models.TextField(db_column=u'151376X1X1', blank=True) # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    def q1data(self):
        q = LimeQuestions.objects.get(qid=1)
        return { 'question': q.question, 'answer':self.q1 }
    q2 = models.CharField(max_length=15, db_column=u'151376X1X2', blank=True) # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    q3 = models.TextField(db_column=u'151376X1X3', blank=True) # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    q4 = models.TextField(db_column=u'151376X1X4', blank=True) # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    q5 = models.TextField(db_column=u'151376X1X5', blank=True) # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    q6 = models.CharField(max_length=15, db_column=u'151376X1X6', blank=True) # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    q6other = models.TextField(db_column=u'151376X1X6other', blank=True) # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    q7 = models.CharField(max_length=15, db_column=u'151376X1X7', blank=True) # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    q8SQ001 = models.CharField(max_length=15, db_column=u'151376X2X8SQ001', blank=True) # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    q10SQ001 = models.CharField(max_length=15, db_column=u'151376X2X10SQ001', blank=True) # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    q10SQ002 = models.CharField(max_length=15, db_column=u'151376X2X10SQ002', blank=True) # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    q10SQ003 = models.CharField(max_length=15, db_column=u'151376X2X10SQ003', blank=True) # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    q10SQ004 = models.CharField(max_length=15, db_column=u'151376X2X10SQ004', blank=True) # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    q10SQ005 = models.CharField(max_length=15, db_column=u'151376X2X10SQ005', blank=True) # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    q10SQ006 = models.CharField(max_length=15, db_column=u'151376X2X10SQ006', blank=True) # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    q17 = models.TextField(db_column=u'151376X2X17', blank=True) # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    q18 = models.TextField(db_column=u'151376X3X18', blank=True) # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    number_151376x3x19 = models.TextField(db_column=u'151376X3X19', blank=True) # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    class Meta:
        db_table = u'lime_survey_151376'

